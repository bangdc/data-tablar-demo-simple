'use strict';

/**
 * @ngdoc overview
 * @name dataTablarApp
 * @description
 * # dataTablarApp
 *
 * Main module of the application.
 */
angular
    .module('dataTablarApp', [
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'dataTablar',
    ])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/data-tablar.html',
                controller: 'MainCtrl',
            })
            .otherwise({
                redirectTo: '/'
            });
    }]);