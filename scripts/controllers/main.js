'use strict';

/**
 * @ngdoc function
 * @name dataTablarApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the dataTablarApp
 */
angular.module('dataTablarApp')
	.controller('MainCtrl', function($scope) {
		$scope.configDataTablar = {
			optionAjaxRequests: [{
				requestConfig: {
					method: 'GET',
					url: 'http://maps.googleapis.com/maps/api/geocode/json',
				},
				typeaheadKey: 'formatted_address',
				conversionRequest: function(requestConfig) {
					// @param {object} response Response object from request. 

					// @returns {object} The final response 
					// Start your conversion function of response from the line below.

					requestConfig.params['address'] = requestConfig.params['typeahead_value'];
					return requestConfig;
					// End your conversion function before this line.

				},
				conversionResponse: function(response) {
					// @param {object} response Response object from request. 

					// @returns {object} The final response 
					// Start your conversion function of response from the line below.

					return {
						dttbResponse: {
							dttbOptions: response.data.results
						},
						dttbSuccess: true
					};
					// End your conversion function before this line.

				},
			}, {
				requestConfig: {
					method: 'GET',
					url: 'http://localhost:5003/departments',
				},
				callAtEachEvent: true,
				callMinimumInterval: 2000,
				conversionResponse: function(response) {
					// @param {object} response Response object from request. 

					// @returns {object} The final response 
					// Start your conversion function of response from the line below.
					return {
						dttbSuccess: true,
						dttbResponse: {
							dttbOptions: response.data
						}
					}

					// End your conversion function before this line.

				},
			}, {
				requestConfig: {
					method: 'GET',
					url: 'http://localhost:5003/colors',
				},
				callAtInit: true,
				conversionResponse: function(response) {
					// @param {object} response Response object from request. 

					// @returns {object} The final response 
					// Start your conversion function of response from the line below.
					return {
						dttbSuccess: true,
						dttbResponse: {
							dttbOptions: response.data
						}
					}

					// End your conversion function before this line.

				},
			}, ],
			table: {
				uniqueId: 'Users-table-1',
				displaySelectRowsPage: true,
				displayPaging: true,
				displayShowingRow: true,
				label: 'Users Table',
				primaryLabel: '',
				displayNavHighlights: true,
				displayNavArrange: true,
				displayNavLocalSort: true,
				displayNavHideShow: true,
				displayNavLanguage: true,
				displayNavExport: true,
				displayNavPrint: true,
				displayNavRefresh: true,
				displayNavShare: true,
				numPagesDisplaying: 5,
				numRowsPerPage: 10,
				numRowsPerPageOptions: [
					10,
					20,
					30,
					40,
					50,
				],
			},
			columns: [{
				dataType: 'number',
				formType: '',
				headerDirection: 'horizontal',
				sortable: true,
				uneditable: true,
				RTLtext: false,
				allowLocalFilter: true,
				label: 'User Id',
				tooltip: '',
				key: 'id',
				displayKey: 'id',
				displayAs: 'ordinal',
				displayAsConfig: {},
				formTypeConfig: {},
			}, {
				dataType: 'text',
				formType: 'input',
				headerDirection: 'horizontal',
				sortable: true,
				uneditable: false,
				RTLtext: false,
				allowLocalFilter: true,
				label: 'First Name',
				tooltip: '',
				key: 'firstName',
				displayKey: 'firstName',
				displayAs: 'text',
				displayAsConfig: {
					textCase: 'capitalizeEach',
				},
				formTypeConfig: {
					displayRequiredStar: true,
					placeholder: 'First Name',
				},
				validations: {
					required: {
						message: 'The First Name is required',
						rule: true,
					},
					minLength: {
						message: 'The length of First Name must be greater than or equal to 6',
						rule: 6,
					},
				},
			}, {
				dataType: 'text',
				formType: 'input',
				headerDirection: 'horizontal',
				sortable: true,
				uneditable: false,
				RTLtext: false,
				allowLocalFilter: true,
				label: 'Email',
				tooltip: '',
				key: 'email',
				displayKey: 'email',
				displayAs: 'email',
				displayAsConfig: {},
				formTypeConfig: {
					displayRequiredStar: true,
					placeholder: 'The email address of user',
				},
				validations: {
					required: {
						message: 'This email address is required',
						rule: true,
					},
				},
			}, {
				dataType: 'date',
				formType: 'datePicker',
				headerDirection: 'horizontal',
				sortable: true,
				uneditable: false,
				RTLtext: false,
				allowLocalFilter: true,
				label: 'Birthday',
				tooltip: '',
				key: 'birthday',
				displayKey: 'birthday',
				displayAs: 'date',
				displayAsConfig: {
					dateFormat: 'YYYY-MM-DD',
				},
				formTypeConfig: {
					displayRequiredStar: true,
				},
				validations: {
					dateSince: {
						message: 'The minimum Birthday is 1990-01-01',
						rule: '1990-01-01',
					},
					dateUntil: {
						message: 'The maximum Birthday is 2016-01-01',
						rule: '2016-01-01',
					},
				},
			}, {
				dataType: 'text',
				formType: 'input',
				headerDirection: 'horizontal',
				sortable: true,
				uneditable: false,
				RTLtext: false,
				allowLocalFilter: true,
				label: 'Username',
				tooltip: '',
				key: 'userName',
				displayKey: 'userName',
				displayAs: 'text',
				displayAsConfig: {},
				formTypeConfig: {
					displayRequiredStar: true,
					placeholder: 'userName',
				},
				validations: {
					required: {
						message: 'The userName is required',
						rule: true,
					},
					minLength: {
						message: 'The length of userName must be greater than or equal to 6',
						rule: 6,
					},
					maxLength: {
						message: 'The length of userName must be less than or equal to 12',
						rule: 12,
					},
				},
			}, {
				dataType: 'text',
				formType: 'input',
				headerDirection: 'horizontal',
				sortable: true,
				uneditable: false,
				RTLtext: false,
				allowLocalFilter: true,
				label: 'Avatar',
				tooltip: '',
				key: 'avatar',
				displayKey: 'avatar',
				displayAs: 'image',
				displayAsConfig: {},
				formTypeConfig: {
					displayRequiredStar: true,
					placeholder: 'The URL to new avatar image',
				},
				validations: {
					required: {
						message: 'This field is required',
						rule: true,
					},
				},
			}, {
				dataType: 'text',
				formType: 'typeahead',
				headerDirection: 'horizontal',
				sortable: true,
				uneditable: false,
				RTLtext: false,
				allowLocalFilter: true,
				label: 'Address',
				tooltip: '',
				key: 'address',
				displayKey: 'address',
				displayAs: 'text',
				displayAsConfig: {
					textCase: 'uppercase',
				},
				formTypeConfig: {
					typeaheadAjaxRequestIndex: 0,
					displayRequiredStar: true,
				},
			}, {
				dataType: 'number',
				formType: 'input',
				headerDirection: 'horizontal',
				sortable: true,
				uneditable: false,
				RTLtext: false,
				allowLocalFilter: true,
				label: 'Salary',
				tooltip: '',
				key: 'salary',
				displayKey: 'salary',
				displayAs: 'currency',
				displayAsConfig: {
					currency: '$',
					fractionSize: 3,
				},
				formTypeConfig: {
					displayRequiredStar: true,
					placeholder: 'Monthly Salary of User',
				},
				validations: {
					greaterThan: {
						message: 'The salary must be greater than 20',
						rule: 20,
					},
					greaterEqual: {
						message: 'The number must be greater than or equal to 40',
						rule: 40,
					},
					equalTo: {
						message: 'The number must be equal to 50',
						rule: 50,
					},
					lessEqual: {
						message: 'The number must be less than or equal to 60',
						rule: 60,
					},
					lessThan: {
						message: 'The number must be less than 80',
						rule: 80,
					},
				},
				highlights: [{
					type: 'bg-primary',
					condition: '>=',
					value: '100',
				}, {
					type: 'bg-info',
					condition: '<',
					value: '100',
				}, ],
			}, {
				dataType: 'boolean',
				formType: 'boolean',
				headerDirection: 'horizontal',
				sortable: true,
				uneditable: false,
				RTLtext: false,
				allowLocalFilter: true,
				label: 'VIP',
				tooltip: '',
				key: 'isVip',
				displayKey: 'isVip',
				displayAs: 'boolean',
				displayAsConfig: {
					booleanAs: 'icon',
					booleanFalseVal: 'fa fa-close',
					booleanTrueVal: 'fa fa-check',
				},
				formTypeConfig: {
					booleanAs: 'default',
				},
				validations: {
					required: {
						message: 'This field is required',
						rule: true,
					},
				},
				highlights: [{
					type: 'bg-primary',
					condition: '===',
					value: true,
				}, {
					type: 'bg-warning',
					condition: '===',
					value: false,
				}, ],
			}, {
				dataType: 'text',
				formType: 'textarea',
				headerDirection: 'horizontal',
				sortable: true,
				uneditable: false,
				RTLtext: false,
				allowLocalFilter: true,
				label: 'About',
				tooltip: '',
				key: 'shortDesc',
				displayKey: 'shortDesc',
				displayAs: 'text',
				displayAsConfig: {
					textCase: 'capitalizeEach',
					limitToLength: 100,
					limitToBegin: 0,
				},
				formTypeConfig: {
					displayRequiredStar: true,
				},
				validations: {
					required: {
						message: 'This field is required',
						rule: true,
					},
					containing: {
						message: 'The text must contain My name is',
						rule: 'My name is',
					},
					maxLength: {
						message: 'The length of text must be less than or equal to 200',
						rule: 200,
					},
					minLength: {
						message: 'The length of text must be greater than or equal to 100',
						rule: 100,
					},
				},
			}, {
				dataType: 'text',
				formType: 'input',
				headerDirection: 'horizontal',
				sortable: true,
				uneditable: false,
				RTLtext: false,
				allowLocalFilter: true,
				label: 'Website',
				tooltip: '',
				key: 'website',
				displayKey: 'firstName',
				displayAs: 'url',
				displayAsConfig: {},
				formTypeConfig: {
					displayRequiredStar: true,
					placeholder: 'Your url',
				},
			}, {
				dataType: 'text',
				formType: 'selectSingle',
				headerDirection: 'horizontal',
				sortable: true,
				uneditable: false,
				RTLtext: false,
				allowLocalFilter: true,
				label: 'Gender',
				tooltip: '',
				key: 'gender',
				displayKey: 'gender',
				displayAs: 'text',
				displayAsConfig: {
					textCase: 'uppercase',
				},
				formTypeConfig: {
					options: [{
						val: 'female',
						label: 'Female',
					}, {
						val: 'male',
						label: 'Male',
					}, {
						val: 'other',
						label: 'Other',
					}, ],
					displayRequiredStar: true,
					optionModelKey: 'val',
					optionDisplayKey: 'label',
				},
				highlights: [{
					type: 'bg-primary',
					condition: '===',
					value: 'male',
				}, {
					type: 'bg-success',
					condition: '===',
					value: 'female',
				}, {
					type: 'bg-info',
					condition: '===',
					value: 'other',
				}, ],
			}, {
				dataType: 'text',
				formType: 'selectSingle',
				headerDirection: 'horizontal',
				sortable: true,
				uneditable: false,
				RTLtext: false,
				allowLocalFilter: true,
				label: 'Department',
				tooltip: '',
				key: 'departmentId',
				displayKey: 'departmentName',
				displayAs: 'text',
				displayAsConfig: {
					textCase: 'uppercase',
				},
				formTypeConfig: {
					optionAjaxRequestIndex: 1,
					displayRequiredStar: true,
					optionModelKey: 'id',
					optionDisplayKey: 'name',
				},
			}, {
				dataType: 'array',
				formType: 'selectMultiple',
				headerDirection: 'horizontal',
				sortable: true,
				uneditable: false,
				RTLtext: false,
				allowLocalFilter: true,
				label: 'Favorite Colors',
				tooltip: '',
				key: 'favoriteColors',
				displayKey: 'favoriteColors',
				displayAs: 'array',
				displayAsConfig: {
					arrayAs: 'text',
				},
				formTypeConfig: {
					optionAjaxRequestIndex: 2,
					displayRequiredStar: true,
					optionModelKey: 'color',
					optionDisplayKey: 'color',
				},
			}, ],
			actions: {
				reading: {
					requestConfig: {
						method: 'GET',
						url: 'http://localhost:5003/users',
					},
					filters: [{
						label: 'First Name',
						placeholder: 'First Name',
						key: 'firstName',
					}, {
						label: 'Email',
						placeholder: 'Email',
						key: 'email',
					}, {
						label: 'Salary',
						placeholder: 'Salary',
						key: 'salary',
					}, {
						label: 'Gender',
						placeholder: 'Gender',
						key: 'gender',
						options: [
							'male',
							'female',
							'other',
						],
					}, ],
					conversionRequest: function(requestConfig) {

						// @param {object} response Response object from request. 

						// @returns {object} The final response 
						// Start your conversion function of response from the line below.
						console.log(requestConfig);
						var dttbFilters = JSON.parse(requestConfig.params.dttbFilters);
						angular.forEach(dttbFilters, function(filter, key) {
							var val = filter.value.toString();
							if (val.length > 0) {
								requestConfig.params[filter.field] = val;
							}
						});
						requestConfig.params['_start'] = parseInt(requestConfig.params.dttbNumRowsPerPage) * (parseInt(requestConfig.params.dttbPage) - 1);
						requestConfig.params['_limit'] = parseInt(requestConfig.params.dttbNumRowsPerPage);
						requestConfig.params['_sort'] = requestConfig.params.dttbSortCol;
						requestConfig.params['_order'] = requestConfig.params.dttbSortDir.toUpperCase();

						delete requestConfig.params.dttbPage;
						delete requestConfig.params.dttbNumRowsPerPage;
						delete requestConfig.params.dttbSortCol;
						delete requestConfig.params.dttbSortDir;
						delete requestConfig.params.dttbFilters;
						return requestConfig;

						// End your conversion function before this line.


					},
					conversionResponse: function(response) {

						// @param {object} response Response object from request. 

						// @returns {object} The final response 
						// Start your conversion function of response from the line below.
						return {
							dttbResponse: {
								dttbRows: response.data,
								dttbNumTotalRows: parseInt(response.headers('X-Total-Count'))

							},
							dttbSuccess: true
						};
						// End your conversion function before this line.


					},
				},
				creating: {
					fields: [{
						dataType: 'text',
						formType: 'input',
						formTypeConfig: {
							displayRequiredStar: true,
							placeholder: 'First Name',
						},
						validations: {
							required: {
								message: 'The First Name is required',
								rule: true,
							},
							minLength: {
								message: 'The length of First Name must be greater than or equal to 6',
								rule: 6,
							},
						},
						key: 'firstName',
						label: 'First Name',
					}, {
						dataType: 'text',
						formType: 'input',
						formTypeConfig: {
							displayRequiredStar: true,
							placeholder: 'The email address of user',
						},
						validations: {
							required: {
								message: 'This email address is required',
								rule: true,
							},
						},
						key: 'email',
						label: 'Email',
					}, {
						dataType: 'date',
						formType: 'datePicker',
						formTypeConfig: {
							displayRequiredStar: true,
						},
						validations: {
							dateSince: {
								message: 'The minimum Birthday is 1990-01-01',
								rule: '1990-01-01',
							},
							dateUntil: {
								message: 'The maximum Birthday is 2016-01-01',
								rule: '2016-01-01',
							},
						},
						key: 'birthday',
						label: 'Birthday',
					}, {
						dataType: 'text',
						formType: 'input',
						formTypeConfig: {
							displayRequiredStar: true,
							placeholder: 'userName',
						},
						validations: {
							required: {
								message: 'The userName is required',
								rule: true,
							},
							minLength: {
								message: 'The length of userName must be greater than or equal to 6',
								rule: 6,
							},
							maxLength: {
								message: 'The length of userName must be less than or equal to 12',
								rule: 12,
							},
						},
						key: 'userName',
						label: 'Username',
					}, {
						dataType: 'text',
						formType: 'input',
						formTypeConfig: {
							displayRequiredStar: true,
							placeholder: 'The URL to new avatar image',
						},
						validations: {
							required: {
								message: 'This field is required',
								rule: true,
							},
						},
						key: 'avatar',
						label: 'Avatar',
					}, {
						dataType: 'text',
						formType: 'typeahead',
						formTypeConfig: {
							typeaheadAjaxRequestIndex: 0,
							displayRequiredStar: true,
						},
						key: 'address',
						label: 'Address',
					}, {
						dataType: 'number',
						formType: 'input',
						formTypeConfig: {
							displayRequiredStar: true,
							placeholder: 'Monthly Salary of User',
						},
						validations: {
							greaterThan: {
								message: 'The salary must be greater than 20',
								rule: 20,
							},
							greaterEqual: {
								message: 'The number must be greater than or equal to 40',
								rule: 40,
							},
							equalTo: {
								message: 'The number must be equal to 50',
								rule: 50,
							},
							lessEqual: {
								message: 'The number must be less than or equal to 60',
								rule: 60,
							},
							lessThan: {
								message: 'The number must be less than 80',
								rule: 80,
							},
						},
						key: 'salary',
						label: 'Salary',
					}, {
						dataType: 'boolean',
						formType: 'boolean',
						formTypeConfig: {
							booleanAs: 'default',
						},
						validations: {
							required: {
								message: 'This field is required',
								rule: true,
							},
						},
						key: 'isVip',
						label: 'VIP',
					}, {
						dataType: 'text',
						formType: 'textarea',
						formTypeConfig: {
							displayRequiredStar: true,
						},
						validations: {
							required: {
								message: 'This field is required',
								rule: true,
							},
							containing: {
								message: 'The text must contain My name is',
								rule: 'My name is',
							},
							maxLength: {
								message: 'The length of text must be less than or equal to 200',
								rule: 200,
							},
							minLength: {
								message: 'The length of text must be greater than or equal to 100',
								rule: 100,
							},
						},
						key: 'shortDesc',
						label: 'About',
					}, {
						dataType: 'text',
						formType: 'input',
						formTypeConfig: {
							displayRequiredStar: true,
							placeholder: 'Your url',
						},
						key: 'website',
						label: 'Website',
					}, {
						dataType: 'text',
						formType: 'selectSingle',
						formTypeConfig: {
							options: [{
								val: 'female',
								label: 'Female',
							}, {
								val: 'male',
								label: 'Male',
							}, {
								val: 'other',
								label: 'Other',
							}, ],
							displayRequiredStar: true,
							optionModelKey: 'val',
							optionDisplayKey: 'label',
						},
						key: 'gender',
						label: 'Gender',
					}, {
						dataType: 'text',
						formType: 'selectSingle',
						formTypeConfig: {
							optionAjaxRequestIndex: 1,
							displayRequiredStar: true,
							optionModelKey: 'id',
							optionDisplayKey: 'name',
						},
						key: 'departmentId',
						label: 'Department',
					}, {
						dataType: 'array',
						formType: 'selectMultiple',
						formTypeConfig: {
							optionAjaxRequestIndex: 2,
							displayRequiredStar: true,
							optionModelKey: 'color',
							optionDisplayKey: 'color',
						},
						key: 'favoriteColors',
						label: 'Favorite Colors',
					}, ],
					requestConfig: {
						method: 'POST',
						url: 'http://localhost:5003/users',
					},
					conversionResponse: function(response) {
						// @param {object} response Response object from request. 

						// @returns {object} The final response 
						// Start your conversion function of response from the line below.

						if (response.status === 201) {
							return {
								dttbSuccess: true,
								dttbResponse: 'Create successfully'
							};
						} else {
							return {
								dttbSuccess: false,
								dttbResponse: 'Fail to create'
							};
						}
						// End your conversion function before this line.

					},
				},
				deleting: {
					requestConfig: {
						method: 'DELETE',
						url: 'http://localhost:5003/users/:id',
					},
					enableRouteParams: true,
					conversionResponse: function(response) {
						// @param {object} response Response object from request. 

						// @returns {object} The final response 
						// Start your conversion function of response from the line below.

						if (response.status === 200) {
							return {
								dttbSuccess: true,
								dttbResponse: 'Delete successfully'
							};
						} else {
							return {
								dttbSuccess: false,
								dttbResponse: 'Fail to delete'
							};
						}
						// End your conversion function before this line.

					},
				},
				updating: {
					onCell: {
						requestConfig: {
							method: 'PATCH',
							url: 'http://localhost:5003/users/:id',
						},
						enableRouteParams: true,
						conversionResponse: function(response) {
							// @param {object} response Response object from request. 

							// @returns {object} The final response 
							// Start your conversion function of response from the line below.
							var result = {};

							if (response.status === 200) {
								result.dttbSuccess = true;
								result.dttbResponse = "Update successfully";
							} else {
								result.dttbSuccess = false;
								result.dttbResponse = "Fail to update";
							}

							return result;
							// End your conversion function before this line.

						},
					},
					onRow: {
						requestConfig: {
							method: 'PUT',
							url: 'http://localhost:5003/users/:id',
						},
						enableRouteParams: true,
						conversionResponse: function(response) {
							// @param {object} response Response object from request. 

							// @returns {object} The final response 
							// Start your conversion function of response from the line below.
							var result = {};

							if (response.status === 200) {
								result.dttbSuccess = true;
								result.dttbResponse = "Update successfully";
							} else {
								result.dttbSuccess = false;
								result.dttbResponse = "Fail to update";
							}

							return result;
							// End your conversion function before this line.
						},
					},
				},
			},
		};
	});